#!/bin/bash
# -*- coding: utf-8; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-

# autopatch.sh: script to manage patches on top of repo
# Copyright (c) 2018, Intel Corporation.
# Author: sgnanase <sundar.gnanasekaran@intel.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.

top_dir=`pwd`
rompath=$(pwd)
vendor_path="android-generic"
utils_dir="$top_dir/vendor/$vendor_path"

ask() {
    # https://djm.me/ask
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}



echo "Searching for vendor patches..."
echo -e ${CL_CYN}""${CL_RST}
while IFS= read -r agvendor_name; do
if [ -d $rompath/vendor/$agvendor_name/ ]; then
  romname=$agvendor_name
  
	if [ -d $utils_dir/patches/google_diff/treble_vendor_patches/$agvendor_name/custom_patches/ ]; then

		while IFS= read -r ag_cst_vendor_name; do
		if [ -d $utils_dir/patches/google_diff/treble_vendor_patches/$agvendor_name/custom_patches/$ag_cst_vendor_name/ ]; then
			customname=$ag_cst_vendor_name
			echo "Found $ag_cst_vendor_name customizations!!"
			echo -e ${CL_CYN}""${CL_RST}
			
			
			if ask "Do you want to use the "$customname" customizations? (This will also rename the build from $agvendor_name to $ag_cst_vendor_name): "; then
				echo -e ${CL_CYN}""${CL_RST}
				echo "you chose Yes"
				echo -e ${CL_CYN}""${CL_RST}
				echo -e ${CL_CYN}"applying $ag_cst_vendor_name patches..."${CL_RST}
				export ROM_VENDOR_CUSTOM_NAME="$ag_cst_vendor_name"
				patch_dir="$utils_dir/patches/google_diff/treble_vendor_patches/$agvendor_name/custom_patches/$ag_cst_vendor_name"
			else
				echo -e ${CL_CYN}""${CL_RST}
				echo "you chose No"
				echo -e ${CL_CYN}""${CL_RST}
				echo "Skipping this customization"
				exit
			fi
			
		else
		  echo "No customizations found"
		  exit
		fi
		done < $utils_dir/patches/google_diff/treble_vendor_patches/$agvendor_name/custom_patches/customizations.lst
	else
	  echo "End of customizations list"

	fi
  
else
  echo ""
fi
done < $rompath/vendor/$vendor_path/gsi_roms.lst

if [$custmname == ""]; then
	echo "No customizations found"
	return 1
fi

private_utils_dir="$top_dir/vendor/$vendor_path/PRIVATE"
private_patch_dir="$private_utils_dir/patches/google_diff/$TARGET_PRODUCT"

current_project=""
previous_project=""
conflict=""
conflict_list=""

apply_patch() {

  pl=$1
  pd=$2

  echo ""
  echo "Applying Customization Patches"

  for i in $pl
  do
    current_project=`dirname $i`
    if [[ $current_project != $previous_project ]]; then
      echo ""
      echo ""
      echo "Trying project $current_project"
    fi
    previous_project=$current_project
    
    if [ -d $top_dir/$current_project ] 
    then
		cd $top_dir/$current_project
		remote=`git remote -v | grep "https://android.googlesource.com/"`
		if [[ -z "$remote" ]]; then
		  default_revision="remotes/m/master"
		else
		  if [[ -f "$top_dir/.repo/manifest.xml" ]]; then
			default_revision=`grep default $top_dir/.repo/manifest.xml | grep -o 'revision="[^"]\+"' | cut -d'=' -f2 | sed 's/\"//g'`
		  else
			echo "Please make sure .repo/manifest.xml"
			# return 1
		  fi
		fi
		
		cd $top_dir/$current_project
		a=`grep "Date: " $pd/$i`
		b=`echo ${a#"Date: "}`
		c=`git log --pretty=format:%aD | grep "$b"`

		if [[ "$c" == "" ]] ; then
		  git am -3 $pd/$i >& /dev/null
		  if [[ $? == 0 ]]; then
			echo "        Applying          $i"
		  else
			echo "        Conflicts          $i"
			git am --abort >& /dev/null
			conflict="y"
			conflict_list="$current_project $conflict_list"
		  fi
		else
		  echo "        Already applied         $i"
		fi
    else
		echo "Project $current_project does not exist"
    fi

  done
}

#Apply common patches
cd $patch_dir
patch_list=`find * -iname "*.patch" | sort -u`
apply_patch "$patch_list" "$patch_dir"

#Apply Embargoed patches if exist
if [[ -d "$private_patch_dir" ]]; then
    echo ""
    echo "Embargoed Patches Found"
    cd $private_patch_dir
    private_patch_list=`find * -iname "*.patch" | sort -u`
    apply_patch "$private_patch_list" "$private_patch_dir"
fi

echo ""
if [[ "$conflict" == "y" ]]; then
  echo "==========================================================================="
  echo "           ALERT : Conflicts Observed while patch application !!           "
  echo "==========================================================================="
  for i in $conflict_list ; do echo $i; done | sort -u
  echo "==========================================================================="
  echo "WARNING: Please resolve Conflict(s). You may need to re-run build..."
  # return 1
else
  echo "==========================================================================="
  echo "           INFO : All patches applied fine !!                              "
  echo "==========================================================================="
fi


